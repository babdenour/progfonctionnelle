package programmation.conctionnel.g_streams;

import java.util.List;
import java.util.stream.Stream;

import com.madhusudhan.wr.allaboutlambdas.domain.Movie;
import com.madhusudhan.wr.allaboutlambdas.util.MovieUtil;

public class CreationDeStreams {

	public static void main(String[] args) {
		CreationDeStreams cs = new CreationDeStreams();
		cs.testCollectionStreams();
		cs.testEmptyStreams();
		cs.testValeurTableauStreams();
		cs.testFileStreams();
		cs.testGeneratedIterateStreams();
	}

	// à partir d 'une collection
	private void testCollectionStreams() {
		List<Movie> moviesNames = MovieUtil.createMovies();
		moviesNames.stream().forEach(System.out::println);
		System.out.println(" -------------- ");

	}

	// stream vide
	private void testEmptyStreams() {
		Stream<Movie> moviesEmptyStreams = Stream.empty();
		System.out.println("testEmptyStreams : " + moviesEmptyStreams.count());
		System.out.println(" -------------- ");
	}

	// à partir d'une combinaison valeur/tableau
	private void testValeurTableauStreams() {
		Stream<String> moviesNames = Stream.of("abdenour", "bahloul");
		System.out.println("testCollectionStreams 1 : " + moviesNames.count());

		//
		String[] tabMovies = { "abdenour", "bahloul" };
		Stream<String> moviesNames2 = Stream.of(tabMovies);
		System.out.println("testCollectionStreams 2 : " + moviesNames2.count());

		System.out.println(" -------------- ");
	}

	// streams infini à partir d'une methode de génération Suplier
	private void testGeneratedIterateStreams() {
		Stream<Double> randomStream = Stream.generate(Math::random);
		// randomStream.forEach(System.out::println);
		//
		Stream<Integer> allNumber = Stream.iterate(1, i -> i + 1);
		allNumber.forEach(System.out::println);
		System.out.println(" -------------- ");
	}

	// à partir d'un fichier
	private void testFileStreams() {

	}

}
