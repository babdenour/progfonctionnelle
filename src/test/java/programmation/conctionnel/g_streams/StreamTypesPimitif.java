package programmation.conctionnel.g_streams;

import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import com.madhusudhan.wr.allaboutlambdas.domain.Trade;
import com.madhusudhan.wr.allaboutlambdas.util.TradeUtil;

public class StreamTypesPimitif {

	public static void main(String[] args) {
		StreamTypesPimitif streamP = new StreamTypesPimitif();
		streamP.testIntStream();
	}

	//
	List<Trade> trades = TradeUtil.createTrades();

	//
	private void testTradeQuantity() {

		Optional<Integer> quantity = trades.stream().map(Trade::getQuantity).reduce(Integer::sum);
	}

	//
	private void testIntStream() {
		int[] tabInts = new int[] { 2, 4, 8 };

		IntStream intStream = IntStream.of(tabInts);
		intStream.forEach(System.out::println);

		IntStream intStream2 = IntStream.of(45, 78, 99);
		intStream2.forEach(System.out::println);

	}

	//
	private void testConvertitStream() {
		int sommeDesQuantity = trades.stream().mapToInt(Trade::getQuantity).sum();
	}

}
