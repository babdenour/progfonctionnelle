package programmation.conctionnel.g_streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.madhusudhan.wr.allaboutlambdas.domain.Movie;
import com.madhusudhan.wr.allaboutlambdas.util.MovieUtil;

public class LazyEtEagerStream {

	public static void main(String[] args) {
		LazyEtEagerStream lestream = new LazyEtEagerStream();
		lestream.testEager();
		lestream.testLazy();
	}

	//
	List<Movie> movies = MovieUtil.createMovies();
	List<Movie> top2Classics = new ArrayList<Movie>(10);

	//
	private void testLazy() {
		// si on enlève le .count on aura aucun affichage dans la console
		// la raison est qu'il y'a deux type de méthodes Lazy et Eager
		// les méthodes lazy comme .filter renvoient un seul stream
		movies.stream().filter(m -> {
			System.out.println("inside lazy operation");
			return m.isClassic() ? true : false;
		}).count();
	}

	//
	private void testEager() {
		//
		Stream<Movie> moviesStream = movies.stream().filter(m -> {
			System.out.println("filter");
			return m.isClassic() ? true : false;
		}).map(m -> {
			System.out.println("map");
			return m;
		});

		System.out.println("--- " + moviesStream.count());

	}
}
