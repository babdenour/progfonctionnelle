package programmation.conctionnel.g_streams;

import java.util.List;
import java.util.stream.Collectors;

import com.madhusudhan.wr.allaboutlambdas.domain.Movie;
import com.madhusudhan.wr.allaboutlambdas.domain.Trade;
import com.madhusudhan.wr.allaboutlambdas.util.MovieUtil;
import com.madhusudhan.wr.allaboutlambdas.util.TradeUtil;

public class StreamsLesBases {

	public static void main(String[] args) {
		StreamsLesBases base = new StreamsLesBases();
		List<Trade> listeTrade = TradeUtil.createTrades();
		//
		List<Trade> listeResultat = base.findLargeTradesUsingPreJava8(listeTrade);
		//
		listeResultat.stream().forEach(t -> System.out.println(t.getId()));
		//
		base.findMoviesByDirector("Steven Spielberg");

	}

	// working with streams
	List<Movie> movies = MovieUtil.createMovies();

	private void findMoviesByDirector(String director) {
		movies.stream().filter(m -> m.getDirector().equals(director) ? true : false).map(Movie::getName).limit(3)
				.forEach(System.out::println);
	}

	//
	private List<Trade> findLargeTradesUsingPreJava8(List<Trade> trades) {
		// avec les streams on se ne pose plus la question du commen le faire (comme
		// avec les boucles) mais la question de quoi j'ai besoin et quand
		// pour rendre un traitement multi threadé il faut juste remplacer le stram par
		// le parallelStream
		List<Trade> largeTrades = trades.stream().filter(trade -> trade.getQuantity() > 10000)
				.filter(Trade::isCancelledTrade).limit(10).collect(Collectors.toList());

		return largeTrades;
	}

}
