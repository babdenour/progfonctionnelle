package programmation.conctionnel.f_interfacesadditionnelles;

import java.util.function.BiFunction;
import java.util.function.Function;

import com.madhusudhan.wr.allaboutlambdas.advancedlambdas.Employee;
import com.madhusudhan.wr.allaboutlambdas.domain.Manager;

public class DeuxFunctionFunction {

	public static void main(String[] args) {
	}

	//
	BiFunction<Employee, Manager, Employee> empMangerBiFunction = (emp, manager) -> emp.getManager()
			.getPersonalAssistant();
	//
	Function<Employee, Employee> empMangerFunction = emp -> emp.getManager().getPersonalAssistant();

	//
	private void testBiFuntion(Employee emp, Manager manager) {
		empMangerBiFunction.apply(emp, manager);
	}

	//
	private void testAndThen(Employee emp, Manager manager) {
		BiFunction<Employee, Manager, Employee> perAssistant = empMangerBiFunction.andThen(empMangerFunction);
	}
}
