package programmation.conctionnel.f_interfacesadditionnelles;

import java.util.function.BiPredicate;

import com.madhusudhan.wr.allaboutlambdas.advancedlambdas.Employee;
import com.madhusudhan.wr.allaboutlambdas.domain.Manager;

public class PredicateAvecDeuxArguments {

	public static void main(String[] args) {
	}

	//
	BiPredicate<Employee, Manager> empMangerPredicate = (emp, manager) -> emp.geManager().equals(manager) ? true
			: false;
	//
	BiPredicate<Employee, Manager> mangerHasAssistantPredicate = (emp,
			manager) -> manager.getPersonalAssistant().equals(emp) ? true : false;

	// AND :pour combiner deux BiPredicate on utilise la méthode AND
	BiPredicate<Employee, Manager> isPA = empMangerPredicate.and(mangerHasAssistantPredicate);

	// OR : pour vérifer que l'un des deux BiPredicate est vrai on utilise la
	// méthode OR
	BiPredicate<Employee, Manager> isPA2 = empMangerPredicate.or(mangerHasAssistantPredicate);

	// NEGATE : pour vérifer la négation d'un BiPredicate utilise la méthode negate
	BiPredicate<Employee, Manager> notAManagerPredicate = empMangerPredicate.negate();

	//
	private void testDeuxPredicate(Employee emp, Manager manager) {

	}

	//
	private void testNegation(Employee emp, Manager manager) {

	}

	//
	private void testAnd(Employee emp, Manager manager) {

	}

}
