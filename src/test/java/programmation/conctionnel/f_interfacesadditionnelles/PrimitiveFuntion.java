package programmation.conctionnel.f_interfacesadditionnelles;

import java.util.function.DoubleConsumer;
import java.util.function.DoubleFunction;
import java.util.function.DoublePredicate;
import java.util.function.DoubleSupplier;
import java.util.function.DoubleToIntFunction;
import java.util.function.Function;
import java.util.function.IntConsumer;
import java.util.function.IntFunction;
import java.util.function.IntPredicate;
import java.util.function.IntSupplier;
import java.util.function.LongConsumer;
import java.util.function.LongFunction;
import java.util.function.LongPredicate;
import java.util.function.LongSupplier;
import java.util.function.Predicate;

import com.madhusudhan.wr.allaboutlambdas.advancedlambdas.Employee;

public class PrimitiveFuntion {

	public static void main(String[] args) {
		int eneNumber = 7;
		boolean test = new PrimitiveFuntion().testPredicate(eneNumber);
		System.err.println(test);
	}

	//
	// Predicate
	Predicate<Integer> enenNumberPredicate = x -> x % 2 == 0;
	// pour les types int il existe une Function IntPredicate
	IntPredicate enenIntNumberPredicate = x -> x % 2 == 0;
	// pour les types double
	DoublePredicate enenDoubleNumberPredicate_1 = x -> x % 2 == 0;
	// qui est la meme que
	Predicate<Double> enenDoubleNumberPredicate_2 = x -> x % 2 == 0;
	// pour les types Long
	LongPredicate enenLongNumberPredicate_1 = x -> x % 2 == 0;
	// qui est la meme que
	Predicate<Long> enenLongNumberPredicate_2 = x -> x % 2 == 0;

	// Consumer
	// il existe aussi des Consumer typé pareil
	IntConsumer intConsumer = null;
	DoubleConsumer doubleConsumer = null;
	LongConsumer longConsumer = null;

	// Supplier
	// il existe aussi des Supplier typé pareil
	IntSupplier intSupplier = null;
	DoubleSupplier doubleSupplier = null;
	LongSupplier longSupplier = null;

	// Funtions
	// On peut utiliser les Funtions primitive avec un objet de retour
	// ce qui revient à dire que le type en entré est un type primitif
	IntFunction<Employee> intFunction = null;
	DoubleFunction<Employee> doubleFunction = null;
	LongFunction<Employee> longFunction = null;

	// Function avec les deux types en entré et sortie en type primitif
	Function<Double, Integer> doubleToInteger1 = null;
	DoubleFunction<Integer> doubleToInteger2 = null;
	DoubleToIntFunction doubleToInteger3 = null;

	//
	private boolean testPredicate(int i) {
		return enenNumberPredicate.test(i);
	}

}
