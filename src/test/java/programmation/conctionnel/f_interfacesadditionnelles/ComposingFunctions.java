package programmation.conctionnel.f_interfacesadditionnelles;

import java.util.function.Function;

import com.madhusudhan.wr.allaboutlambdas.advancedlambdas.Employee;
import com.madhusudhan.wr.allaboutlambdas.domain.Manager;

public class ComposingFunctions {

	public static void main(String[] args) {

	}

	// l'interface Function contient aussi deuc autres méthodes
	// 1. compose
	// 2. identity
	Function<Employee, Manager> managerFinder = (emp) -> getManager(emp);
	Function<Manager, Employee> assistantFinder = (manager) -> getPersonalAssistant(manager);

	//
	private Manager getManager(Employee emp) {
		return emp.geManager();
	}

	//
	private Employee getPersonalAssistant(Manager manager) {
		// return manager.getPersonalAssistant();
		return null;
	}

	// andThen method
	private void testAndThen(Employee emp) {
		// dans ce cas le type en entré de l'inerface Function est le meme que celui de
		// sorie et avec le andThen il y'a un ordre de gauche à droite
		// on pense alors a la méthode copose --> voir ci-dessous
		Function<Employee, Employee> empManagerAssistantFinder = managerFinder.andThen(assistantFinder);
	}

	// compose method
	private void testCompose(Employee emp) {
		// avec la méthode compose l'ordre est de droite à gauche
		// c'est le résultat de la méthode managerFinder qui sera l'argument de la
		// méthode assistantFinder
		Function<Employee, Employee> empManagerAssistantFinder = assistantFinder.compose(managerFinder);
	}

	//
	private void testIdentity() {
		Function<String, String> id = Function.identity();
		String result = id.apply("ID12EFL");
		System.out.println("result : " + result);
	}
}
