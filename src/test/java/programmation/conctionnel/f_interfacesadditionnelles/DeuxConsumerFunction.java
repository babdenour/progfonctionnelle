package programmation.conctionnel.f_interfacesadditionnelles;

import java.util.function.BiConsumer;

import com.madhusudhan.wr.allaboutlambdas.advancedlambdas.Employee;

public class DeuxConsumerFunction {

	public static void main(String[] args) {
		Employee emp = new Employee(10);
		int bonus = 5;
		int salaryHike = 15;
		new DeuxConsumerFunction().testBiConsumer(emp, bonus, salaryHike);

	}

	//
	BiConsumer<Employee, Integer> empBonusConsumer = (emp, bonus) -> System.out
			.printf("employee %s is set of %d pct of bonus : ", emp, bonus);

	//
	BiConsumer<Employee, Integer> empSalaryHikeConsumer = (emp, sal) -> System.out
			.printf("employee %s is recieving  %d hike in salary  : ", emp, sal);

	// Pour combiner les deux BiConsumer
	BiConsumer<Employee, Integer> empBonusAndSalaryHikeConsumer = empBonusConsumer.andThen(empSalaryHikeConsumer);

	//
	private void testBiConsumer(Employee emp, Integer bonus, Integer salaryHike) {
		empBonusConsumer.accept(emp, bonus);
	}

}
