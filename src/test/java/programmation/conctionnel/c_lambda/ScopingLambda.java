package programmation.conctionnel.c_lambda;

import com.madhusudhan.wr.allaboutlambdas.workingwithlambdas.SuperScope;

public class ScopingLambda extends SuperScope {

	public static void main(String[] args) {
		new ScopingLambda().testMember(" petit fils ");
	}

	//
	private String member = "fils";

	//
	interface Family {
		String who(String member);
	}

	//
	public void testMember(String memeber) {
		System.out.println("Local memeber : " + memeber);
		System.out.println("Local memeber : " + this.member);
		System.out.println("Local memeber : " + super.member);

		// les variables dans une lambda expression ont exactement le meme scope que
		// les variables qui ne sont pas dans une lambda expression
		//
		System.out.println(" ------------------- ");
		//
		Family familyLambda = (memberFamily) -> {
			System.out.println("Local memeber : " + memberFamily);
			System.out.println("Local memeber : " + memeber);
			System.out.println("Local memeber : " + this.member);
			System.out.println("Local memeber : " + super.member);
			return memberFamily;
		};
		//
		familyLambda.who(memeber);

	}

}
