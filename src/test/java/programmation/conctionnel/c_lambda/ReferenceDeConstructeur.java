package programmation.conctionnel.c_lambda;

public class ReferenceDeConstructeur {

	public static void main(String[] args) {

	}

	//
	class Movie {
		public Movie(int id) {
		}

		public Movie(int id, String name) {
		}
	}

	//
	interface MovieFactory {
		public Movie create(int id);
	}

	//
	MovieFactory mf1 = i -> new Movie(i);
	MovieFactory mf2 = Movie::new;

	// pour utiliser le deuxième constructeur il faut créer une nouvelle interface
	interface MovieFactory_2 {
		public Movie create(int id, String name);
	}

	//
	MovieFactory_2 mf2_1 = (i, nom) -> new Movie(i, nom);
	MovieFactory_2 mf2_2 = Movie::new;

}
