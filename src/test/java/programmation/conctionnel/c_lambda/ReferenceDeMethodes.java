package programmation.conctionnel.c_lambda;

public class ReferenceDeMethodes {

	public static void main(String[] args) {
		ReferenceDeMethodes client = new ReferenceDeMethodes();
	}

	//
	interface IMovie {
		public boolean check(int id);
	}

	// static method refrence usage
	public void testMovieStaticUsage() {
		IMovie m1 = (i) -> i < 100 ? true : false;
		// appem par référence de méthode
		IMovie m2 = ReferenceDeMethodes::isClassic;
	}

	// instance method references usage
	private void testMovieInstanceMethodRef() {
		ReferenceDeMethodes ref = new ReferenceDeMethodes();
		IMovie m1 = (i) -> i > 10 && i < 100 ? true : false;
		//
		IMovie m2 = ref::isTop10;

	}

	// appel d'une méthode par référence dans une classe externe
	private void testMovieArbitraryObjectMethod() {
		IMovie m1 = AutresMethodesDeReference::isComedy;
	}

	//
	public static boolean isClassic(int movieId) {
		return true;
	}

	//
	public boolean isTop10(int movieId) {
		return true;
	}

}

/**
 * Classe d'autres méthodes possibles
 * 
 */
class AutresMethodesDeReference {
	public static boolean isComedy(int i) {
		if (i == 10) {
			return true;
		}
		return false;
	}
}
