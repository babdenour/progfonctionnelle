package programmation.conctionnel.c_lambda;

public class TableauDeReference {

	public static void main(String[] args) {

	}

	interface StringArray {
		String[] create(int size);
	}

	private void testArrayConstructReferences() {
		StringArray sArray1 = (size) -> new String[size];
		StringArray sArray2 = String[]::new;
	}
}
