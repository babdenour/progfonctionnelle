package programmation.conctionnel.c_lambda;

import com.madhusudhan.wr.allaboutlambdas.domain.Trade;

public class CapturingLambda {

	public static void main(String[] args) {
		CapturingLambda cl = new CapturingLambda();
		System.out.println("is open Trade using non-capturing lambda " + cl.isOpenTrade());
	}

	//
	Trade trade = new Trade("IBM", 20000, "OPEN");

	//
	public interface ITrade {
		public boolean check(Trade t);
	}

	public boolean isOpenTrade() {
		ITrade simpleLambda = (t) -> t.isOpen() ? true : false;
		return simpleLambda.check(trade);
	}

	public boolean checkStatus(String status) {
		ITrade statusLambda = (t) -> t.getStatus().equals(status) ? true : false;
		return statusLambda.check(trade);
	}

	// les variables locales sont toutes final par défaut
	// ci-dessous un exemple pour utiliser une variable globale (d'instance) dans
	// une lambda
	// expression
	int count = 0;

	public void instanceVariable(int localCount) {
		for (int i = 0; i < count; i++) {
			ITrade calculLambda = t -> {
				// on incrémente une variable d instance
				count++;
				// localCount++; --> ERROR variable finale
				return t.isOpen();
			};
		}
	}
}
