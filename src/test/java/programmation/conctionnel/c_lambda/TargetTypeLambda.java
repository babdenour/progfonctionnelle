package programmation.conctionnel.c_lambda;


public class TargetTypeLambda {

	public static void main(String[] args) {
		TargetTypeLambda targetTypeLambda = new TargetTypeLambda();
		System.out.println(targetTypeLambda.getEmail("Abdenour", (String nom) -> nom + " :) @gmail.com"));
	}

	// Email email = (String nom) -> nom + "ZZZZZZZZZZZZZ@gmail.com";

	public String getEmail(String nom, Email email) {
		return "le nom = " + nom + " l email = " + email.contstruireEmail(nom);
	}

	public interface Email {
		String contstruireEmail(String nom);
	}
}
