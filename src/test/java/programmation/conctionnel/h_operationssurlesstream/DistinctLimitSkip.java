package programmation.conctionnel.h_operationssurlesstream;

import java.util.List;
import java.util.stream.Stream;

import com.madhusudhan.wr.allaboutlambdas.advancedlambdas.Employee;
import com.madhusudhan.wr.allaboutlambdas.util.EmployeeUtil;

public class DistinctLimitSkip {

	public static void main(String[] args) {
		DistinctLimitSkip dls = new DistinctLimitSkip();
		dls.testSkip();
	}

	//
	List<Employee> employees = EmployeeUtil.createEmployees();

	//
	private void testDistinct() {
	}

	//
	private void testLimit() {
	}

	//
	private void testSkip() {
		Stream<String> empStream = employees.stream().map(Employee::getName);
		empStream.forEachOrdered(System.out::println);

		System.out.println(" -------------- ");

		Stream<String> empStream2 = employees.stream().skip(2).map(Employee::getName);
		empStream2.forEachOrdered(System.out::println);

	}

}
