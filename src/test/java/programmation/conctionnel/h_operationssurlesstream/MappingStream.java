package programmation.conctionnel.h_operationssurlesstream;

import java.util.List;
import java.util.stream.Stream;

import com.madhusudhan.wr.allaboutlambdas.advancedlambdas.Employee;
import com.madhusudhan.wr.allaboutlambdas.util.EmployeeUtil;

public class MappingStream {

	public static void main(String[] args) {
		MappingStream fs = new MappingStream();
		fs.testMapping();
	}

	//
	List<Employee> employees = EmployeeUtil.createEmployees();

	//
	private void testMapping() {
		Stream<String> empStream = employees.stream().map(Employee::getName).map(String::toUpperCase);
		empStream.forEachOrdered(System.out::println);

	}

}
