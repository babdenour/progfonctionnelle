package programmation.conctionnel.h_operationssurlesstream;

import java.util.List;
import java.util.Optional;

import com.madhusudhan.wr.allaboutlambdas.domain.Trade;
import com.madhusudhan.wr.allaboutlambdas.util.TradeUtil;

public class FindersAndMatching {

	public static void main(String[] args) {
		FindersAndMatching fm = new FindersAndMatching();
		// la déférence entre findAny et findFirst c'est que la finAny renvoie le
		// premier qu'elle sans vérifier les autres occurrences à l inverse de findFirst
		fm.testFindAny();
		fm.testFindFirst();
		fm.testAnyMatch();
		fm.testAllMatch();
		fm.testNoneMatch();
	}

	//
	List<Trade> trades = TradeUtil.createTrades();

	//
	private void testFindFirst() {
		Optional<Trade> findFirstTrade = trades.stream().filter(Trade::isBigTrade).findFirst();
		System.out.println("testFindFirst  : " + findFirstTrade.get());
		System.out.println(" -----------------  ");
	}

	//
	private void testFindAny() {
		Optional<Trade> anyTrade = trades.stream().filter(Trade::isBigTrade).findAny();
		System.out.println("testFindAny  : " + anyTrade.get());
		System.out.println(" -----------------  ");
	}

	//
	private void testAnyMatch() {
		boolean anyMatchROTTEN = trades.stream().anyMatch(t -> t.getStatus().equals("ROTTEN"));
		System.out.println("testAnyMatch  : " + anyMatchROTTEN);
		System.out.println(" -----------------  ");
	}

	//
	private void testAllMatch() {
		boolean allMatchIBM = trades.stream().allMatch(t -> t.getInstrument().equals("IBM"));
		System.out.println("testAllMatch  : " + allMatchIBM);
		System.out.println(" -----------------  ");
	}

	//
	private void testNoneMatch() {
		boolean noneMatchIBM = trades.stream().noneMatch(t -> t.getInstrument().equals("IBM"));
		System.out.println("testNoneMatch  : " + noneMatchIBM);
		System.out.println(" -----------------  ");
	}

}
