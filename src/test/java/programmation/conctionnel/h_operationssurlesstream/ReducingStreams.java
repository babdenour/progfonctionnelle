package programmation.conctionnel.h_operationssurlesstream;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.madhusudhan.wr.allaboutlambdas.domain.Trade;
import com.madhusudhan.wr.allaboutlambdas.util.TradeUtil;

public class ReducingStreams {

	public static void main(String[] args) {
		ReducingStreams rs = new ReducingStreams();
		rs.testTradeInstrumentList();
		rs.testTradeQuantitySum();
		rs.testSchoolStaf();

	}

	//
	List<Trade> trades = TradeUtil.createTrades();

	//
	private void testTradeInstrumentList() {

		Optional<String> instList = trades.stream().map(Trade::getInstrument).reduce((a, b) -> a + "," + b);
		System.out.println("testTradeInstrumentList  : " + instList.get());
		System.out.println(" -----------------  ");
	}

	//
	private void testTradeQuantitySum() {

		Optional<Integer> quantitySum = trades.stream().map(Trade::getQuantity).reduce((a, b) -> a + b);
		System.out.println("testTradeQuantitySum  : " + quantitySum.get());
		System.out.println(" -----------------  ");
	}

	//
	private void testSchoolStaf() {

		List<Integer> ints = Arrays.asList(1, 99);
		int staff = ints.stream().reduce(77, (i, j) -> i + j);
		System.out.println("testSchoolStaf  : " + staff);
		System.out.println(" -----------------  ");
	}
}
