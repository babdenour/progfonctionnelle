package programmation.conctionnel.h_operationssurlesstream;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.madhusudhan.wr.allaboutlambdas.domain.Student;
import com.madhusudhan.wr.allaboutlambdas.domain.Trade;
import com.madhusudhan.wr.allaboutlambdas.util.TradeUtil;

public class OptionalStream {

	public static void main(String[] args) {
		OptionalStream os = new OptionalStream();
		// partie 1
		os.attendeesOptional();
		os.NO_attendeesOptional();
		// partie 2
		os.creatingOptional();
		os.ifElseOptional();
		try {
			os.ifElseThrowOptionals();
		} catch (Exception e) {
			e.printStackTrace();
		}

		os.filterMapoOptional();

	}

	//
	List<Trade> trades = TradeUtil.createTrades();

	//
	private void attendeesOptional() {

		List<Integer> attendees = Arrays.asList(1, 99);
		Optional<Integer> attendeesOptional = attendees.stream().reduce(Integer::sum);
		System.out.println("attendeesOptional  : " + attendeesOptional.get());
		System.out.println(" -----------------  ");
	}

	//
	private void NO_attendeesOptional() {

		// cette fois la liste est vide
		//
		List<Integer> attendees = Arrays.asList();
		Optional<Integer> noattendeesOptional = attendees.stream().reduce(Integer::sum);
		// la méthode reduce ne renvoie aucune fonction optional puisqu'il n'ya aucun
		// élément dans la liste
		// on vérifie cela avec la méthode isPresent
		if (noattendeesOptional.isPresent())
			System.out.println("NO_attendeesOptional  : " + noattendeesOptional.get());
		System.out.println(" -----------------  ");
	}

	//
	//

	private void creatingOptional() {

		Student student = new Student();
		Optional<Student> studentOptional = Optional.of(student);
		System.out.println(" Student  :  " + studentOptional.get());

		System.out.println(" -----------------  ");
		studentOptional.ifPresent(System.out::println);
		System.out.println(" -----------------  ");

		student = null;
		Optional<Student> studentOptional2 = Optional.ofNullable(student);
		System.out.println(" Student can t be null :  " + studentOptional2);
		System.out.println(" -----------------  ");
		studentOptional2.ifPresent(System.out::println);
	}

	//
	private void ifElseOptional() {
		Student student = null;
		Student defaultStudent = new Student();
		defaultStudent.setName("Abdenour");
		//
		Optional<Student> studentOptional = Optional.ofNullable(student);
		String name = studentOptional.orElse(defaultStudent).getName();
		System.out.println(" Student  Name  :  " + name);
		System.out.println(" -----------------  ");
	}

	//
	private void ifElseThrowOptionals() throws Exception {
		Student student = null;
		//
		Optional<Student> studentOptional = Optional.ofNullable(student);
		studentOptional.orElseThrow(Exception::new);
		System.out.println(" -----------------  ");
	}

	//
	private void filterMapoOptional() {
		Student student = new Student();
		student.setName("BAHLOUL");
		//
		Optional<Student> studentOptional = Optional.of(student);
		studentOptional.filter(s -> s.hasTeacher()).ifPresent(System.out::println);
		System.out.println(" filterMapoOptional -----------------  ");
		studentOptional.map(s -> s.getName()).ifPresent(System.out::println);
	}

}
