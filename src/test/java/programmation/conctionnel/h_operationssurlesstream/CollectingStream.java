package programmation.conctionnel.h_operationssurlesstream;

import static java.util.stream.Collectors.toMap;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.madhusudhan.wr.allaboutlambdas.domain.Movie;
import com.madhusudhan.wr.allaboutlambdas.domain.Trade;
import com.madhusudhan.wr.allaboutlambdas.util.MovieUtil;
import com.madhusudhan.wr.allaboutlambdas.util.TradeUtil;

public class CollectingStream {

	public static void main(String[] args) {
		CollectingStream cs = new CollectingStream();
		cs.testCollectIntoAList();
		cs.testCollectIntoASet();
		cs.testCollectIntoAMap();
	}

	//
	List<Trade> trades = TradeUtil.createTrades();

	//
	private void testCollectIntoAList() {
		List<Trade> tradeCollection = trades.stream().collect(Collectors.toList());
		tradeCollection.stream().forEach(System.out::println);
		System.out.println("  -----------------   ");
	}

	//
	private void testCollectIntoASet() {
		Set<Trade> tradeCollection = trades.stream().collect(Collectors.toSet());
		tradeCollection.stream().forEach(System.out::println);
		System.out.println("  -----------------   ");
	}

	//
	private void testCollectIntoAMap() {
		// pour les maps il faut une clé et une valeur
		List<Movie> movies = MovieUtil.createMovies();
		Map<String, Movie> movieMap = movies.stream().collect(toMap(Movie::getName, movie -> movie));
		System.out.println(movieMap);
		System.out.println("  -----------------   ");
	}

}
