package programmation.conctionnel.h_operationssurlesstream;

import java.util.List;
import java.util.stream.Stream;

import com.madhusudhan.wr.allaboutlambdas.advancedlambdas.Employee;
import com.madhusudhan.wr.allaboutlambdas.util.EmployeeUtil;

public class FiltrerLesStreams {

	public static void main(String[] args) {
		FiltrerLesStreams fs = new FiltrerLesStreams();
		fs.testFiltre();
	}

	//
	List<Employee> employees = EmployeeUtil.createEmployees();

	//
	private void testFiltre() {
		Stream<Employee> empStream = employees.stream().filter(Employee::isSenior).filter(Employee::isExecutive);
		empStream.forEachOrdered(System.out::println);

	}
}
