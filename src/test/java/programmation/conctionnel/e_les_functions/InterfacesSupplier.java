package programmation.conctionnel.e_les_functions;

import java.util.function.Supplier;

public class InterfacesSupplier {

	public static void main(String[] args) {
		new InterfacesSupplier().testSupplier();
	}

	// Les interfaces Supplier prennent aucun parametre en entré et renvoient un
	// objet du meme type
	Supplier<String[]> fruitSupplier = () -> new String[] { "Pomme", "Orange" };

	//
	private void testSupplier() {
		String[] fruits = fruitSupplier.get();
		for (String fruit : fruits) {
			System.out.println("le fruit est : " + fruit);
		}
	}

}
