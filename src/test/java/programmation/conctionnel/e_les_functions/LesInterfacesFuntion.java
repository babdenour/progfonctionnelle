package programmation.conctionnel.e_les_functions;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import com.madhusudhan.wr.allaboutlambdas.domain.Movie;
import com.madhusudhan.wr.allaboutlambdas.domain.Trade;
import com.madhusudhan.wr.allaboutlambdas.util.TradeUtil;

public class LesInterfacesFuntion {
	public static void main(String[] args) {
		new LesInterfacesFuntion().testFunction("La bataille d Alger.", 3);
	}

	// les interfaces functions prenne deux objets en paramètre font un traitement
	// et renvoie un objet qui a le meme type que le deuxième objet en entré
	Function<String, Movie> createMovieFuntion = s -> new Movie(s);

	//
	List<Trade> trades = TradeUtil.createTrades();

	Function<Integer, Trade> traideFinder = id -> {
		for (Trade trade : trades) {
			if (trade.getId() == id)
				return trade;
		}
		return new Trade();
	};
	// On combine Suppplier et Function
	Supplier<List<Trade>> listeTradeAvecSupplier = () -> TradeUtil.createTrades();
	// meme Funtion mais avec le supplier
	Function<Integer, Trade> traideFinder2 = id -> {
		List<Trade> listeTrades = listeTradeAvecSupplier.get();
		return listeTrades.stream().filter(t -> t.getId() == id).findFirst().get();
	};

	//
	private void testFunction(String movieName, Integer i) {
		Movie movie = createMovieFuntion.apply(movieName);
		System.out.println("Movie is : " + movie);

		System.out.println(" -------------- ");

		Trade trade = traideFinder.apply(i);
		System.out.println("Movie is : " + trade);

		System.out.println(" -------------- ");

		Trade trade2 = traideFinder2.apply(i + 1);
		System.out.println("Movie is : " + trade2);
	}
}
