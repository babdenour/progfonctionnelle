package programmation.conctionnel.e_les_functions;

import java.util.function.Consumer;

import com.madhusudhan.wr.allaboutlambdas.domain.Movie;

public class InterfaceConsummer {

	public static void main(String[] args) {
		Movie movie = new Movie("la bataille d Alger");
		new InterfaceConsummer().testConsumer(movie);
		System.out.println(" --------------------  ");
		new InterfaceConsummer().testAndThen(movie);
	}

	// les interfaces consumer ont une seule méthode accept
	Consumer<Movie> printInfo = m -> System.out.println("Printing out movie info : " + m);
	// exemple on persiste l objet movie en base
	Consumer<Movie> persistMovie = m -> persist(m);

	//
	private void testConsumer(Movie movie) {
		printInfo.accept(movie);
		persistMovie.accept(movie);
	}

	//
	private void persist(Movie movie) {
		System.out.println("On persist l objet en base " + movie);
	}

	// il existe pour l interface Consumer une méthode andThen pour executer
	// successivement une méthode accept l une aprés l autre
	private void testAndThen(Movie movie) {
		Consumer<Movie> printAndPersit = persistMovie.andThen(printInfo);
		printAndPersit.accept(movie);
	}

}
