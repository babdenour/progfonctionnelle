package programmation.conctionnel;

/**
 * @author abdenourB
 * @since Oct 29, 2019
 * 
 */
public class GreetingLambdas {

	/**
	 * Cette interfaces représente le body de l'expression lambda
	 * 
	 */
	interface Greeting {
		public String sayHello(String g);
	}

	public void testGreeting(String a, Greeting g) {
		String result = g.sayHello(a);
		System.out.println("Resultat : " + result);
	}

	public static void main(String[] args) {
		// (String s) -> " Salam " + s : on donne comme input une chaine de caractere
		// comme dans la declaratin de greeting mais dans le body de sayHello on donne :
		// " Salam " + s
		new GreetingLambdas().testGreeting("Abdenour ", (String s) -> " Salam " + s);
	}
}
