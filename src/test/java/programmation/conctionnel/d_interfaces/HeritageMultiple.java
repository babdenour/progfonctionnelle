package programmation.conctionnel.d_interfaces;

public class HeritageMultiple {

	public static void main(String[] args) {

	}

	//
	interface Engine {
		// méthode 1
		default String methodeAvecMemeNom(int id) {
			return "default model";
		}
	}

	//
	interface Vehicle {
		// méthode 2
		default String methodeAvecMemeNom(int id) {
			return "default model";
		}
	}

	//
	class Car implements Engine, Vehicle {
		// méthode 3
		// il faut re-définir les méthode qui ont les mémes noms et signature dans cette
		// classe sinon ERROR

		public String methodeAvecMemeNom(int id) {
			// pour appeler une des méthdes aves le meme nom de l'interface implémentée il
			// faut utiliser le mot super
			Engine.super.methodeAvecMemeNom(id);
			Vehicle.super.methodeAvecMemeNom(id);
			return "default model";
		}
	}
}
