package programmation.conctionnel.d_interfaces;

public class HeritageComportement {

	public static void main(String[] args) {

	}

	//
	interface Engine {
		default String make() {
			return "default make";
		}
	}

	//
	interface Vehicle {
		default String model() {
			return "default model";
		}
	}

	//
	class Car implements Engine, Vehicle {
		String makeAndModel() {
			return Engine.super.make() + Vehicle.super.model();
		}
	}

}
