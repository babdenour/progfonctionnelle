package programmation.conctionnel.d_interfaces;

public class InterfacesFonctionnelles {

	public static void main(String[] args) {

	}

	// interface fonctionnelle
	// une interface fonctionnelle doit avoir une seule méthode abstraite
	@FunctionalInterface
	interface Multiplicateur {
		int multiplier(int i, int j);
	}

	//
	Multiplicateur multiplicateur = (a, b) -> (a - b) * 10;
}
