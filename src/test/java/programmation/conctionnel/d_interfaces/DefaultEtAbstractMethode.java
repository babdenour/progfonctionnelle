package programmation.conctionnel.d_interfaces;

public class DefaultEtAbstractMethode {

	@FunctionalInterface
	interface Employee {
		// méthode abstraite
		Employee find(int id);

		// méthode default
		default boolean isExec(int id) {
			//
			return true;
		}

		// méthode static
		static String getDefaultCountry() {
			return "DZ";
		}
	}

	public static void main(String[] args) {
		class EmployeeImpl implements Employee {

			@Override
			public Employee find(int id) {
				// appel de la méthode default
				// la méthode n'est pas overridée
				boolean executive = isExec(id);
				Employee.getDefaultCountry();
				return null;
			}
		}
		//
		EmployeeImpl emp = new EmployeeImpl();
		emp.isExec(1234);
		//
		String defaultCountry = Employee.getDefaultCountry();
	}

}
