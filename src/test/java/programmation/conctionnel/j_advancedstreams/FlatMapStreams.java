package programmation.conctionnel.j_advancedstreams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import com.madhusudhan.wr.allaboutlambdas.domain.Actor;
import com.madhusudhan.wr.allaboutlambdas.domain.Movie;
import com.madhusudhan.wr.allaboutlambdas.util.MovieUtil;

public class FlatMapStreams {

	public static void main(String[] args) {
		FlatMapStreams fs = new FlatMapStreams();
		fs.testFlapMovies();
		fs.testFlapVeggies();
	}

	//
	List<Movie> movies = MovieUtil.createMoviesAndActors();
	String[] friuts = new String[] { "Pomme", "Orange" };
	String[] veggies = new String[] { "Beans", "Peas" };

	//
	private void testFlapMovies() {
		// la méthode flatMap est équivalente à une double boucle for sur une liste
		// d'objets et un attribut de cette objets qui lui est une liste
		// getActors() --> renvoie la liste des acteurs pour la movie en cours
		Stream<Actor> actorStream = movies.stream().flatMap(m -> m.getActors().stream());
		actorStream.forEach(System.out::println);
		System.out.println("----------------------- ");

	}

	//
	private void testFlapVeggies() {
		Stream<List<String>> fruitsAndVeggies = Stream.of(Arrays.asList(friuts), Arrays.asList(veggies));
		fruitsAndVeggies.forEach(System.out::println);

		System.out.println("----------------------- ");
		Stream<List<String>> fruitsAndVeggies2 = Stream.of(Arrays.asList(friuts), Arrays.asList(veggies));
		fruitsAndVeggies2.flatMap(s -> s.stream()).forEach(System.out::println);
		System.out.println("----------------------- ");

	}

}
