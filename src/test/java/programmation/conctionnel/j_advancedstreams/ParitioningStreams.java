package programmation.conctionnel.j_advancedstreams;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.madhusudhan.wr.allaboutlambdas.advancedlambdas.Employee;
import com.madhusudhan.wr.allaboutlambdas.util.EmployeeUtil;

public class ParitioningStreams {

	public static void main(String[] args) {
		ParitioningStreams ps = new ParitioningStreams();
		ps.partitioningByExecutive();
		ps.partitioningAndGroup();
	}

	//
	List<Employee> employees = EmployeeUtil.createEmployees();

	//
	private void partitioningByExecutive() {
		Map<Boolean, List<Employee>> employeePartition = employees.stream()
				.collect(Collectors.partitioningBy(Employee::isExecutive));
		System.out.println("partitioningByExecutive : " + employeePartition);
		System.out.println("----------------------- ");

	}

	//
	private void partitioningAndGroup() {
		Map<Boolean, Map<String, List<Employee>>> employeePartitionAndGroup = employees.stream().collect(
				Collectors.partitioningBy((Employee::isExecutive), Collectors.groupingBy(Employee::getDepartment)));
		System.out.println("partitioningAndGroup : " + employeePartitionAndGroup);
		System.out.println("----------------------- ");

	}
}
