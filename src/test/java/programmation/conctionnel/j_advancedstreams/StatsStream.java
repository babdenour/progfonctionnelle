package programmation.conctionnel.j_advancedstreams;

import java.util.OptionalInt;
import java.util.stream.IntStream;

public class StatsStream {
	public static void main(String[] args) {
		StatsStream sst = new StatsStream();
		sst.range();
		sst.testMaxAndMin();
	}

	//
	IntStream streamOfInts = IntStream.range(1, 100);

	//
	private void range() {
		IntStream ints = IntStream.range(10, 20);
		ints.forEach(System.out::println);
		//
		System.out.println("----------------------- ");
		//
		IntStream ints2 = IntStream.rangeClosed(10, 20);
		ints2.forEach(System.out::println);
		System.out.println("----------------------- ");
	}

	//
	private void testMaxAndMin() {
		//
		IntStream ints2 = IntStream.rangeClosed(10, 20);
		OptionalInt max = ints2.max();
		System.out.println("Max = " + max.getAsInt());
		System.out.println("----------------------- ");
		ints2 = IntStream.rangeClosed(10, 20);
		OptionalInt min = ints2.min();
		System.out.println("Min = " + min.getAsInt());
		System.out.println("----------------------- ");
		ints2 = IntStream.rangeClosed(10, 20);
		OptionalInt avg = ints2.min();
		System.out.println("Moyenne = " + avg.getAsInt());
	}

}
