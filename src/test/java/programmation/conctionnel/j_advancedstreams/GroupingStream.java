package programmation.conctionnel.j_advancedstreams;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.madhusudhan.wr.allaboutlambdas.advancedlambdas.Employee;
import com.madhusudhan.wr.allaboutlambdas.util.EmployeeUtil;

public class GroupingStream {

	public static void main(String[] args) {
		GroupingStream gs = new GroupingStream();
		gs.groupByDepartment();
		gs.groupByDepartmentAndExcec();
	}

	//
	List<Employee> employees = EmployeeUtil.createEmployees();

	//
	private void groupByDepartment() {
		Map<String, List<Employee>> mapEmployeeByDepartment = employees.stream()
				.collect(Collectors.groupingBy(e -> e.getDepartment()));
		System.out.println("groupByDepartment : " + mapEmployeeByDepartment);
		System.out.println("----------------------- ");

	}

	// multi level grouping by
	private void groupByDepartmentAndExcec() {
		Map<String, Map<String, List<Employee>>> mapEmployeeByDepartmentAndCity = employees.stream()
				.collect(Collectors.groupingBy((Employee::getDepartment), Collectors.groupingBy(Employee::getCity)));
		System.out.println("groupByDepartmentAndExcec : " + mapEmployeeByDepartmentAndCity);
		System.out.println("----------------------- ");
	}
}
