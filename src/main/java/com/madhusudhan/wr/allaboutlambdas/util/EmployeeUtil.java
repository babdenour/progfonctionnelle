package com.madhusudhan.wr.allaboutlambdas.util;

import java.util.ArrayList;
import java.util.List;

import com.madhusudhan.wr.allaboutlambdas.advancedlambdas.Employee;
import com.madhusudhan.wr.allaboutlambdas.domain.Employee___2;
import com.madhusudhan.wr.allaboutlambdas.domain.Manager;

public class EmployeeUtil {
	public static List<Employee> createEmployees() {

		List<Employee> employees = new ArrayList<>();

		Manager manager1 = new Manager("Sarah James");
		Manager manager2 = new Manager("Elizabeth Williamson");

		Employee emp1 = new Employee(1, "Harry Barry", "Finance", true, "Alger");
		emp1.setManager(manager1);
		emp1.setSenior(true);
		employees.add(emp1);

		Employee emp2 = new Employee(2, "Hasro Biko", "IT", false, "PARIS");
		emp2.setSenior(true);
		employees.add(emp2);

		emp2.setManager(manager2);

		Employee emp3 = new Employee(3, "Ram Nivas", "Strategy", true, "Alger");
		employees.add(emp3);
		emp3.setManager(manager1);

		Employee emp4 = new Employee(4, "John Bhaijan", "Marketing", true, "PARIS");
		emp4.setSenior(true);
		employees.add(emp4);
		emp4.setManager(manager2);

		Employee emp5 = new Employee(5, "Kirk Carlos", "Marketing", false, "PARIS");
		employees.add(emp5);
		emp5.setManager(manager2);

		Employee emp6 = new Employee(6, "Kirk Carlos", "Marketing", false, "Alger");
		employees.add(emp6);
		emp5.setManager(manager2);

		return employees;

	}

	public static List<Employee___2> createDetailedEmployees() {

		List<Employee___2> employees = new ArrayList<>();

		Manager manager1 = new Manager("Sarah James");
		Manager manager2 = new Manager("Elizabeth Williamson");

		Employee___2 emp1 = new Employee___2(0, "Harry Barry", "Finance", "London");
		emp1.setExecutive(true);
		emp1.setSenior(true);
		emp1.setManager(manager1);
		employees.add(emp1);

		Employee___2 emp2 = new Employee___2(1, "Hasro Biko", "IT", "New York");
		emp2.setExecutive(false);
		emp2.setSenior(false);
		employees.add(emp2);
		emp2.setManager(manager2);

		Employee___2 emp3 = new Employee___2(2, "Ram Nivas", "Strategy", "London");
		employees.add(emp3);
		emp3.setExecutive(true);
		emp3.setSenior(true);
		emp3.setManager(manager1);

		Employee___2 emp4 = new Employee___2(3, "John Bhaijan", "Marketing", "Hyderabad");
		employees.add(emp4);
		emp4.setExecutive(true);
		emp4.setSenior(true);
		emp4.setManager(manager2);

		Employee___2 emp5 = new Employee___2(3, "Kirk Carlos", "Marketing", "New York");
		employees.add(emp5);
		emp5.setExecutive(false);
		emp5.setSenior(true);
		emp5.setManager(manager2);

		Employee___2 emp6 = new Employee___2(3, "Kirk Carlos", "Marketing", "New York");
		employees.add(emp5);
		emp6.setExecutive(false);
		emp6.setSenior(true);
		emp5.setManager(manager2);

		return employees;

	}

}
