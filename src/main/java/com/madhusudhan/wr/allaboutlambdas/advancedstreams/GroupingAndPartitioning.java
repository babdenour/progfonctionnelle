package com.madhusudhan.wr.allaboutlambdas.advancedstreams;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.partitioningBy;

import com.madhusudhan.wr.allaboutlambdas.domain.Employee___2;
import com.madhusudhan.wr.allaboutlambdas.util.EmployeeUtil;

/**
 * This class demonstrates the usage of groupingBy function
 * 
 * @author mkonda
 *
 */
public class GroupingAndPartitioning {

	List<Employee___2> employees = EmployeeUtil.createDetailedEmployees();

	/**
	 * Grouping by department function
	 */
	private void groupingByDepartment() {
		Map<String, List<Employee___2>> deptEmployees = employees
			.stream()
			.collect(groupingBy(e -> e.getDepartment()));
		
		System.out.println(deptEmployees);
	}

	/**
	 * Grouping by city
	 */
	private void groupingByCity() {
		Map<String, List<Employee___2>> cityEmployees = employees
			.stream()
			.collect(Collectors.groupingBy(Employee___2::getCity));
		System.out.println(cityEmployees);
	}
	
	/**
	 * Multi-level grouping usage: group employees by department and city
	 */
	private void groupingByDeptAndCity() {
		Map<String, Map<String, List<Employee___2>>> deptAndCityEmployees =
				employees.stream()
		.collect(groupingBy((Employee___2::getDepartment), groupingBy(Employee___2::getCity)));
		System.out.println(deptAndCityEmployees);
	}
	
	private void partitionByExecutives() {
		Map<Boolean, List<Employee___2>> empPartition = employees
				.stream()
				.collect(Collectors.partitioningBy(Employee___2::isExecutive));
	
		System.out.println(empPartition);
	}
	
	private void partitioningAndGrouping() {
		Map<Boolean, Map<String, List<Employee___2>>> execEmployees = employees
			.stream()
			.collect(partitioningBy((Employee___2::isExecutive), groupingBy(Employee___2::getDepartment)));
		
		for(Boolean b: execEmployees.keySet()){
			System.out.println(b+" --> "+execEmployees.get(b));
		}
	}
	
	private void multiLevelPartitioning() {
		Map<Boolean, Map<Boolean, List<Employee___2>>> execEmployees = employees
			.stream()
			.collect(
				partitioningBy((Employee___2::isExecutive), 
				partitioningBy(Employee___2::isSenior)));
		
		for(Boolean b: execEmployees.keySet()){
			System.out.println(b+":"+execEmployees.get(b));
		}

	}
	public static void main(String[] args) {
//		new GroupingAndPartitioning().groupingByDepartment();
//		new GroupingAndPartitioning().groupingByCity();
//		new GroupingAndPartitioning().groupingByDeptAndCity();
//		new GroupingAndPartitioning().partitionByExecutives();
//		new GroupingAndPartitioning().partitioningAndGrouping();
		new GroupingAndPartitioning().multiLevelPartitioning();
	}
}
