package com.madhusudhan.wr.allaboutlambdas.specialisedfunctions;

import java.util.function.BiFunction;

import com.madhusudhan.wr.allaboutlambdas.domain.Employee___2;
import com.madhusudhan.wr.allaboutlambdas.domain.Manager;

/**
 * Class that demonstrates Predicate function usage
 * 
 * @author mkonda
 *
 */
public class BiFunctions {

	BiFunction<Employee___2, Manager, Employee___2> empManagerBiFunction = (emp, manager) -> {
		Employee___2 employee = null;
		// if(emp.getManager().equals(manager))
		// employee = manager.getPersonalAssistant();
		return employee;
	};

	// Function<Employee___2, Employee___2> emplManagerFunction = emp ->
	// emp.getManager().getPersonalAssistant();

	// Single argument function
	// Function<Employee___2, Employee___2> empManagerFunction = emp ->
	// emp.getManager().getPersonalAssistant();

	private void biFunction(Employee___2 emp, Manager manager) {
		Employee___2 employee = empManagerBiFunction.apply(emp, manager);
		System.out.println("Employee" + employee);
	}

	private void testAndThen(Employee___2 emp, Manager manager) {
		// BiFunction<Employee___2, Manager, Employee___2> personalAssistant =
		// empManagerBiFunction
		// .andThen(empManagerFunction);
	}

	public static void main(String[] args) {
		Employee___2 emp = new Employee___2(99);
		Manager manager = new Manager();
		// emp.setManager(manager);
		// manager.setPersonalAssistant(emp);
		new BiFunctions().biFunction(emp, manager);
	}

}
