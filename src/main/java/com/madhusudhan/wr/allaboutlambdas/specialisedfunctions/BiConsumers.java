package com.madhusudhan.wr.allaboutlambdas.specialisedfunctions;

import java.util.function.BiConsumer;

import com.madhusudhan.wr.allaboutlambdas.domain.Employee___2;

/**
 * Class that demonstrates Predicate function usage
 * 
 * @author mkonda
 *
 */
public class BiConsumers {
	
	BiConsumer<Employee___2, Integer> empBonusConsumer = 
			(emp, bonus) -> System.out.printf("Employee %s is set of %d pct of bonus:", emp, bonus);
			
	BiConsumer<Employee___2, Integer> empSalHikeConsumer = (emp, sal) ->
		System.out.printf("Employee %s is receiving %d hike in salary", emp, sal);
		
	
	
	private void testBiConsumer(Employee___2 emp, Integer bonus, Integer salaryHike) {
		empBonusConsumer.accept(emp, bonus);
	}

	private void usingAndThen() {
		// BiConsumer
		BiConsumer<Employee___2, Integer> empBonusAndHikeConsumer = empBonusConsumer.andThen(empSalHikeConsumer);
	}
	
	public static void main(String[] args) {
		Employee___2 emp = new Employee___2(10);
		int bonus = 5;
		int salaryHike = 15;
		new BiConsumers().testBiConsumer(emp, bonus, salaryHike);
	}

}
