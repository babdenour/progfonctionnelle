package com.madhusudhan.wr.allaboutlambdas.advancedlambdas;

import com.madhusudhan.wr.allaboutlambdas.domain.Address;
import com.madhusudhan.wr.allaboutlambdas.domain.Manager;

/**
 * Class to demonstrate the usage of constructor references
 * 
 * @author mkonda
 *
 */
public class Employee {

	private int id = 0;
	private String name = null;
	private String department = null;
	private String city = null;
	private Address address = null;
	private Manager manager = null;
	private boolean isExecutive = false;
	private boolean senior = false;
	private int ratings = 10;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	public boolean isExecutive() {
		return isExecutive;
	}

	public void setExecutive(boolean isExecutive) {
		this.isExecutive = isExecutive;
	}

	public boolean isSenior() {
		return senior;
	}

	public void setSenior(boolean senior) {
		this.senior = senior;
	}

	public void setRatings(int ratings) {
		this.ratings = ratings;
	}

	/**
	 * An Employee created using an id
	 * 
	 * @param id
	 */
	public Employee(int id) {
		// Logic for creating an employee with an id
	}

	/**
	 * An Employee created using an id and a name
	 * 
	 * @param id
	 * @param name
	 */
	public Employee(int id, String name) {
		// Logic for creating an employee with an id and name.
	}

	public Employee(int i, String string, String string2, boolean b, String city) {
		setId(i);
		setName(string);
		setDepartment(string2);
		setExecutive(b);
		setSenior(true);
		setCity(city);
	}

	/**
	 * Interface representing the first constructor
	 * 
	 * @author mkonda
	 *
	 */
	interface EmployeeById {
		public Employee create(int id);
	}

	/**
	 * Interface representing the second constructor
	 * 
	 * @author mkonda
	 *
	 */
	interface EmployeeByName {
		public Employee create(int id, String employee);
	}

	/**
	 * Lambdas without using constructor references
	 */
	public void normalLambdaExpressions() {
		// Lambda invoking the first constructor - no const refs
		EmployeeById empLambda = id -> new Employee(id);

		// Lambda invoking the second constructor - no const refs
		EmployeeByName empNameLambda = (id, name) -> new Employee(id, name);
	}

	/**
	 * Lambdas with constructor references
	 */
	public void constructorReferencedLambdas() {
		// Both have the same constructor reference

		// Constructor reference invocation for first constructor
		EmployeeById empLambdaConstRef = Employee::new;
		// Constructor reference invocation for second constructor
		EmployeeByName empNameLambdaConstRef = Employee::new;

	}

	public int getRatings() {
		return 10;
	}

	public Manager geManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		// return "Employee [id=" + id + ", name=" + name + ", address=" +
		// address
		// + "]";

		return "Employee " + id;
	}
}
